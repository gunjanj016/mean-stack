# mean stack

Realtime Chat Example with Angular9, Node.js, Socket.io and mongoDB.

I am using:
**Angular9**
**Node** 
**Express**
**Socket.io**

**Runtime instructions**
first dowmload the clome and extract.
-In main have 2saprate folder for backend and frontend.
**For Backend-**

*  Run npm install to install all dependency.
*  I have the root app.js node script pointing to serve.
*  Run node npm start from within the project root to start the Node server on port 3000.

**For FrontEnd**

*  Run npm install in frondend folder.
*  Run ng serve or npm start to run angular app.
*  Browse to http://localhost:3000 and you should see the chat client using minified scripts.
